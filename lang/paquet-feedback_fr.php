<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// Fichier source, a modifier dans svn://zone.spip.org/spip-zone/_core_/plugins/breves/lang/
if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// B
	'feedback_description' => 'Les feedback sont des informations courtes, sans auteur.',
	'feedback_slogan' => 'Gestion des feedback dans SPIP'
);

?>
