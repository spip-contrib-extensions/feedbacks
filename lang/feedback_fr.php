<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// Fichier source, a modifier dans svn://zone.spip.org/spip-zone/_core_/plugins/feedback/lang/
if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// E
	'entree_feedback_publiee' => 'Ce feedback est :',
	'entree_texte_feedback' => 'Texte du feedback',

	// F
	'feedback' => 'Feedback',
	'feedback_anonyme' => 'Afin de protéger votre confidentialité, veuillez vous assurer qu\'aucune information permettant votre identification ne figure dans votre envoi.',
	'feedback_anonyme_erreur' => 'Vous ne devez pas insérer d\'information personnelle dans votre texte',
	'feedback_attention_cent_caracteres'  => 'Vous ne pouvez pas dépasser 100 caractères',
	'feedback_attention_dix_caracteres' => 'Votre feedback doit faire plus de 10 caractères',
	'feedback_choix' => 'Votre choix',
	'feedback_envoyer' => 'Feedback envoyé',
	'feedback_idee' => 'idée',
	'feedback_idee_choix' => 'Vous avez une idée?',
	'feedback_journaliers' => 'Feedback de la journée',
	'feedback_negatif' => 'problème',
	'feedback_negatif_choix' => 'Vous avez un problème?',
	'feedback_nb_idee' => 'idées',
	'feedback_nb_positif' => 'èloges',
	'feedback_nb_negatif' => 'problèmes',
	'feedback_positif' => 'èloge',
	'feedback_positif_choix' => 'Oui, c\'est plutôt pas mal!',
	'feedback_statistiques' => 'Statistiques feedback',
	'feedback_tous' => 'Tous les feedback',

	// I
	'icone_feedback' => 'Feedback',
	'icone_ecrire_nouvel_article' => 'Les feedback contenues dans cette rubrique',
	'icone_modifier_feedback' => 'Modifier ce feedback',
	'icone_nouveau_feedback' => 'Écrire un nouveau feedback',
	'info_1_feedback' => '1 Feedback',
	'info_aucun_feedback' => 'Aucune feedback',
	'info_description_abbreviation' => 'détail du feedback',
	'info_feedback' => 'Votre site utilise-t-il le système de feedback ?',
	'info_feedback_02' => 'Feedback',
	'info_feedback_valider' => 'Feedback à valider',
	'info_gauche_numero_feedback' => 'Feedback numéro',
	'info_nb_feedback' => '@nb@ feedback',
	'item_feedback_proposee' => 'proposée à l\'évaluation',
	'item_feedback_refusee' => 'refusée',
	'item_feedback_validee' => 'validée',
	'item_non_utiliser_feedback' => 'Ne pas utiliser les feedback',
	'item_utiliser_feedback' => 'Utiliser les feedback',

	// L
	'liste_feedback' => 'Liste des feedback',
	'logo_feedback' => 'Logo des feedback',

	// P
	'plus_options' => 'Plus d\'options',

	// T
	'texte_feedback' => 'Les feedback sont des textes courts envoyés par les utilisateurs, il peut vous paraître intéressant de connaitre l\'avis de vos visiteurs sur votre site...',
	'titre_feedback_proposee' => 'Feedback proposée',
	'titre_feedback_publiee' => 'Feedback publiée',
	'titre_feedback_refusee' => 'Feedback refusée',
	'titre_feedback' => 'Les feedback',
	'titre_langue_feedback' => 'Langue des feedback',
	'titre_nouvelle_feedback' => 'Nouveau feedback',
	'titre_page_feedback' => 'Feedback',
	'twitter_feedback' => 'Dites-le sur Tweeter'
);

?>
